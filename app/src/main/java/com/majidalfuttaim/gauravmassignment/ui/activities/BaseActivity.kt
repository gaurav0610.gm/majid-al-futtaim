package com.majidalfuttaim.gauravmassignment.ui.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.inflate
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import com.majidalfuttaim.gauravmassignment.R
import com.majidalfuttaim.gauravmassignment.databinding.ActivityBaseBinding

abstract class BaseActivity<T: ViewDataBinding>: FragmentActivity() {

    lateinit var mActivityBinding: T

    private var mBaseActivityBinding: ActivityBaseBinding? = null

    @LayoutRes
    protected abstract fun getLayoutResourceId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBaseActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
        mActivityBinding = inflate(inflater, getLayoutResourceId(), (mBaseActivityBinding?.flContainer), true)
    }

    fun getBaseActivity():ActivityBaseBinding? {
        return mBaseActivityBinding
    }

    fun getContainerActivity(): T {
        return mActivityBinding
    }

    fun showOverlayProgress(){
       // mBaseActivityBinding?.constraintLayoutProgress?.visibility = VISIBLE
    }

    fun hideOverlayProgress(){
        //mBaseActivityBinding?.constraintLayoutProgress?.visibility = INVISIBLE
    }

    fun showToast(msg:String){
        Toast.makeText(applicationContext, msg, LENGTH_SHORT).show()
    }

}
package com.majidalfuttaim.gauravmassignment.ui.bindings

import android.content.Intent
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

class CommonBindings {

    @BindingAdapter("share")
    fun share(textView:TextView, url:String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "Hey Check out this awesome repo. \n $url")
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        textView.context.startActivity(shareIntent)
    }

    @BindingAdapter("visible")
    fun share(textView:TextView, isVisible:Boolean) {
        if(isVisible)
            textView.visibility = View.GONE
        else
            textView.visibility = View.VISIBLE
    }
}
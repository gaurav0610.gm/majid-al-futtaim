package com.majidalfuttaim.gauravmassignment.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.majidalfuttaim.gauravmassignment.ui.interfaces.GenericRecyclerInterface

open class GenericRecyclerAdapter<T, LVM : ViewDataBinding> : RecyclerView.Adapter<GenericRecyclerAdapter<T, LVM>.RecyclerViewHolder> {

    private var items: ArrayList<T>? = null
    private var layoutId: Int = 0
    private lateinit var bindingInterface: GenericRecyclerInterface<LVM, T>

    constructor(items: ArrayList<T>) {
        this.items = items
    }

    constructor(items: ArrayList<T>,layoutId: Int, bindingInterface: GenericRecyclerInterface<LVM, T>) {
        this.items = items
        this.layoutId = layoutId
        this.bindingInterface = bindingInterface
    }


    inner class RecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var binding: LVM = DataBindingUtil.bind<LVM>(view)!!

        fun bindData(model: T, pos: Int) {
            bindingInterface.bindData(binding, model, pos)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(layoutId, parent, false)

        val viewHolder: RecyclerViewHolder
        viewHolder = RecyclerViewHolder(v)
        return viewHolder
    }


    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = items!![position]
        holder.bindData(item, position)
    }


    override fun getItemCount(): Int {
        return if (items == null) {
            0
        } else items!!.size
    }
}
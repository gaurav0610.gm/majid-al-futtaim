package com.majidalfuttaim.gauravmassignment.backend.retrofit

import com.majidalfuttaim.gauravmassignment.BuildConfig
import com.majidalfuttaim.gauravmassignment.backend.api.GithubApi
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()


    internal fun getGitlabApi(): GithubApi {
        return retrofit.create(GithubApi::class.java)
    }
}
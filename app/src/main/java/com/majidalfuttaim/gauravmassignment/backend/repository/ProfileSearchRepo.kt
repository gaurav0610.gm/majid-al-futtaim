package com.majidalfuttaim.gauravmassignment.backend.repository

import com.majidalfuttaim.gauravmassignment.backend.api.GithubApi
import com.majidalfuttaim.gauravmassignment.backend.model.Repo
import com.majidalfuttaim.gauravmassignment.backend.retrofit.BaseDataSource
import com.majidalfuttaim.gauravmassignment.backend.retrofit.Result
import com.majidalfuttaim.gauravmassignment.backend.retrofit.RetrofitService

object ProfileSearchRepo:BaseDataSource() {

    var githubApi:GithubApi = RetrofitService.getGitlabApi();

    suspend fun searchUserOnId(userNameId: String): Result<List<Repo>> = getResult {
        githubApi.searchUserOnId(userNameId)
    }
}